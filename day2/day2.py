def rotate(str, n):
    return str[n:] + str[:n]


def points(result, my_shape):
    return result * 3 + my_shape + 1


with open('./day2/data.txt', 'r') as f:
    total_shapes = 0
    total_responses = 0
    for line in f.readlines():
        total_shapes += points(
            rotate('ZXY', 'ABC'.index(line[0])).index(line[2]),
            'XYZ'.index(line[2])
        )
        total_responses += points(
            'XYZ'.index(line[2]),
            'ABC'.index(
                rotate('CAB', 'ABC'.index(line[0]))[
                    'XYZ'.index(line[2])]
            )
        )

print(total_shapes)
print(total_responses)
