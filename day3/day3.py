# import string library function 
import string


def get_misplaced(lines):
    misplaced = []
    for line in lines:
        rucksack = items_placement(line)
        misplaced.append(find_misplaced(rucksack))
    return sum_values(misplaced)


def get_groups(lines):
    badges = []
    for line in range(0, len(lines), 3):
        badges.append(find_badge([lines[line], lines[line+1], lines[line+2]]))
    return sum_values(badges)


def items_placement(line: str) -> list[str, str]:
    half = len(line) // 2
    first = line[half:]
    second = line[:half]
    return [first, second]


def find_misplaced(rucksack: list[str, str]) -> str:
    return (set(rucksack[0]).intersection(rucksack[1])).pop()


def get_priority(letter: str) -> int:
    priority_order = string.ascii_lowercase + string.ascii_uppercase
    return priority_order.index(letter) + 1


def sum_values(items: list[str]) -> int:
    sum = 0
    for item in items:
        sum += get_priority(item)
    return sum


def find_badge(lines: list[str, str, str]) -> str:
    return (set(lines[0]) & set(lines[1]) & set(lines[2])).pop()


with open('./day3/data.txt', 'r') as data:
    lines = data.read().splitlines()
    print(get_misplaced(lines))
    print(get_groups(lines))
