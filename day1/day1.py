elves = []


def order_elves():
    calories = 0
    with open('./day1/data.txt', 'r') as f:
        for line in f.read().splitlines():
            if line != '':
                calories += int(line)
            else:
                elves.append(calories)
                calories = 0


def sum_calories(quantity):
    elves_order = sorted(elves)[::-1]
    calories = 0
    for order in range(quantity):
        calories += elves_order[order]
    return calories


order_elves()
print(sum_calories(3))
