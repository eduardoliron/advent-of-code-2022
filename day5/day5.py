import re


def get_columns(lines):
    crates = [[], [], [], [], [], [], [], [], []]
    for line in range(8):
        current_line = get_letter(lines[line])
        for column in range(0, len(current_line)):
            if current_line[column] != ' ':
                crates[column].append(current_line[column])
    return crates


def get_letter(string):
    return [string[i+1:i+2] for i in range(0, len(string), 4)]


def move_crates(lines, columns):
    for line in range(10, len(lines)):
        move_rules = re.findall('[0-9]+', lines[line])
        print(move_rules)


with open('./day5/data.txt', 'r') as data:
    lines = data.read().splitlines()
    columns = get_columns(lines)
    move_crates(lines, columns)
