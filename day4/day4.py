def get_sectors(line: str) -> list[list[int], list[int]]:
    sectors_list = []
    sectors = line.split(",")
    for sector in sectors:
        sectors_list.append(get_ranges(sector))
    return sectors_list


def get_ranges(sector_list: str) -> list[list[int], list[int]]:
    range_limits = sector_list.split("-")
    return set(range(int(range_limits[0]), int(range_limits[1]) + 1))


def is_overlapping(assigned_sectors: list[list[int], list[int]]):
    return len(assigned_sectors[0] & assigned_sectors[1]) != 0


def is_containing(assigned_sectors: list[list[int], list[int]]):
    return assigned_sectors[0].issubset(assigned_sectors[1]) or assigned_sectors[1].issubset(assigned_sectors[0])


with open('./day4/data.txt', 'r') as data:
    contained_count = 0
    overlapping_count = 0
    for line in data.read().splitlines():
        if is_containing(get_sectors(line)):
            contained_count += 1
        if is_overlapping(get_sectors(line)):
            overlapping_count += 1
    print('contained:', contained_count)
    print('overlapping:', overlapping_count)
